---
title: Blog
stylesheets: [
    "/styles/style_blog.css"
]

cascade:
  stylesheets:
    - "/styles/style_posts.css"
---
