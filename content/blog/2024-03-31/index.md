---
title: Third music album
image: "/blog/2024-03-31/background.svg"
---

It's so much fun writing music, so much so that I couldn't stop working on a new one, today I am proud to finally release it to the public: [Tears in Rain](/music/tears_in_rain/).  

All the 10 tracks are made again using only [LMMS](https://lmms.io/), from the sounds of Africa to the electric guitars, you get to experience a plethora of different sounds, wishing you to feel less alone in this world.
Listen to the album and enjoy the tracks, they are released under the CC-BY license.  
