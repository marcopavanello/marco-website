---
title: My first music album
image: "/blog/2022-08-31/background.svg"
---

I've been working on a music album since May, so today I finally release it: [First Steps](/music/first_steps/).
All the 10 tracks are made using only [LMMS](https://lmms.io/), among the used instruments there are strings, choir, synthesizers, electric guitars, drums and others...

It has been a fun project to work on, and I really enjoyed converting my inner emotions into sound vibrations, so I might make another album in the future, who knows.
Listen to the album and enjoy the tracks, they are released under the CC-BY license.
