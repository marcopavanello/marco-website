---
title: Open Source Alternatives
image: "/blog/2022-05-21/background.svg"
---

Here is a list of Free and Open Source alternative services to the proprietary counterparts:


Windows, macOS -> Ubuntu, Fedora, Arch Linux...  
Android -> Graphene OS  
Whatsapp -> Signal  
Facebook, Instagram, Twitter... -> Mastodon  
YouTube -> PeerTube  
GitHub -> GitLab  
Discord -> Element  
Google -> DuckDuckGo  
Google Chrome -> Firefox  
Gmail -> Protonmail  
Google Drive -> CryptDrive  
Google Keep -> Standard Notes  
Google Maps -> OpenStreetMap  
Google Calendar -> Simple Calendar  
Google Domains -> Njalla  
Microsoft Office, Google Office... -> Libre Office  
Maya, Autodesk, 3DS Max... -> Blender  
Unreal Engine, Unity... -> Godot  
Photoshop -> Gimp  
Adobe Illustrator -> Inkscape  
