---
title: Second music album
image: "/blog/2023-04-26/background.svg"
---

Here we go with a second one, couldn't stop expanding my personal journey into music.  
I've been working on a second music album since the release of the first one, and today I am proud to finally release it to the public: [Butterflies](/music/butterflies/).  

All the 10 tracks are made again using only [LMMS](https://lmms.io/), with a whole new palette of sounds all over the place. Some tracks are really experimental because I wanted to investigate the forbidden possibilities of sound design, pushing myself into unknown territory.  
Listen to the album and enjoy the tracks, they are released under the CC-BY license.  

Hopefully this one will make you feel the butterflies inside your chest.