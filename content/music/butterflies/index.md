---
title: Butterflies
cover: "/music/butterflies/cover.webp"
year: 2023
duration: 25 min 52 sec
download: https://drive.proton.me/urls/WTZRNEH2TR#GcRMh6FeNs35
---

## Description

This is my second music album, completely created only by using LMMS again.  
With different investigations in sound design, it comes with a whole new palette of colors of sound, hope you feel the butterflies inside your chest. Enjoy!  
<br>
**Different Approach** is using marimba, bass, strings, percussions and choir.  
**Butterflies** is using piano, bells, strings and synthesizers.  
**Fake Belief** is using only synthesizers.  
**Stronger Than Life** is using trumpets, strings, choir and drums.  
**Infinity** is using synthesizers and violins.  
**Just Wait** is using guitars, strings, french horns and drums.  
**Time Flies** is using whispers and strings.  
**Requiem** is using choir and organ.  
**Pure Chaos** is using drums, piano, bells, basses, flutes and other instruments.  
**A New Beginning** is using only a piano.
{.paragraph}
