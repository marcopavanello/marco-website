---
title: First Steps
cover: "/music/first_steps/cover.webp"
year: 2022
duration: 29 min 15 sec
download: https://drive.proton.me/urls/FD1NRTVDR8#VOeK4eKiKRQR
---

## Description

This is my first music album, completely created only by using LMMS.  
Music is said to be therapeutic, I really believe so, this is why all the tracks here are made such to evoke an emotional response to humans who listen to them. Enjoy!  
<br>
**Track 1** is using choirs, drums and synthesizers.  
**Track 2** is using violins, cellos and synthesizers.  
**Track 3** is using electric guitars, drums, hihats and synthesizers.  
**Track 4** is using bells, choirs and synthesizers.  
**Track 5** is using only synthesizers.  
**Track 6** is using a piano, strings, choirs and bells.  
**Track 7** is using strings, electric guitar, bells and choir.  
**Track 8** is using piano, strings and choir.  
**Track 9** is using choir and strings.  
**Track 10** is using only synthesizers.
{.paragraph}
