---
title: Tears in Rain
cover: "/music/tears_in_rain/cover.webp"
year: 2024
duration: 29 min 25 sec
download: https://drive.proton.me/urls/RB59KXNS3M#8j8UJGlG6HFP
---

## Description

Here it comes a third one, this album explores deeper different genres and emotions, from the ethnical sounds of Africa to the prickly chords of electric guitars.  
If you feel alone or empty, this is the right album for you, lonely soul.  
Again, every piece is designed to evoke emotions and feelings that are simply impossible to transmit through other mediums.  
Enjoy!
{.paragraph}
