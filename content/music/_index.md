---
title: Music

sort: music

stylesheets: [
    "/styles/style_music.css"
]

cascade:
  stylesheets:
    - "/styles/style_album.css"
---
